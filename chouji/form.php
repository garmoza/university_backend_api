<?php
    $errors = getArrayErrors();
    
    if (count($errors) > 0) {
        setcookie('errors', json_encode($errors), time() + 24 * 60 * 60);
       
        setcookie('wrong_info', json_encode( array('Name' => $_POST['Name'],
                                                   'E_mail' => $_POST['E_mail'],
                                                   'Date' => $_POST['Date'],
                                                   'Gender' => $_POST['Gender'],
                                                   'Number_of_limbs' => $_POST['Number_of_limbs'],
                                                   'Superpowers' => implode(', ', $_POST['Superpowers']),
                                                   'Biografia' => $_POST['Biografia'],
                                                   'Accept' => empty($_POST['Accept']) ? false : true ) 
            ), time() + 24 * 60 * 60);
        
        header('Location: http://u15643.kubsu-dev.ru/chouji/');
        
        exit();
    } 
    
    setcookie('errors', '', 1000);
    
    setcookie('wrong_info', '', 1000);
    
    setcookie('success_info', json_encode( array('Name' => $_POST['Name'],
        'E_mail' => $_POST['E_mail'],
        'Date' => $_POST['Date'],
        'Gender' => $_POST['Gender'],
        'Number_of_limbs' => $_POST['Number_of_limbs'],
        'Superpowers' => implode(', ', $_POST['Superpowers']),
        'Biografia' => $_POST['Biografia'],
        'Accept' => empty($_POST['Accept']) ? false : true )
      ), time() + 24 * 60 * 60 * 365);
    
    addInDB();
    
    header('Location: http://u15643.kubsu-dev.ru/chouji/?success');

    function getArrayErrors() {
        $errors = array();
        
        if ( preg_match('/^$/', $_POST['Name']) ) { 
            $errors['Name'] = 'Empty "Name" field';
        } else if (strlen($_POST['Name']) > 20) {
            $errors['Name'] = '"Name" is too long';
        } else if ( !preg_match('/^[a-zA-Z]{1}[a-zA-Z]*$/', $_POST['Name']) ) {
            $errors['Name'] = '"Name" field must consist only of letters';
        }
        
        if( preg_match('/^$/', $_POST['E_mail']) ) {
            $errors['E_mail'] = 'Empty "Email" field';
        } else if (strlen($_POST['E_mail']) > 320) {
            $errors['E_mail'] = '"Email" is too long';
        } else if ( !preg_match('/[^\s]+@[^\s\.]+\.[a-z]+/', $_POST['E_mail']) ) {
            $errors['E_mail'] = 'Invalid format for the "Email" field. Right format for example: example@mail.com';
        }
        
        if ( count($_POST['Superpowers']) == 0) {
            $errors['Superpowers'] = '"Superpower" is not selected';
        }
        
        if ( preg_match('/^$/', $_POST['Biografia']) ) {
            $errors['Biografia'] = 'Empty "Biografia" field';
        } else if (strlen($_POST['Biografia']) > 320) {
            $errors['Biografia'] = '"Biografia" is too long';
        }
        
        if (empty($_POST['Accept'])) {
            $errors['Accept'] = 'The privacy policy has not been accepted';
        }
            
            return $errors;
    }
    
    function addInDB() {
        $connection = 'mysql:host=localhost;dbname=u15643';
        $pdo = new PDO($connection, 'u15643', '2705239');
        
        $sql = 'INSERT INTO temari SET Name = ?, E_mail = ?, Date = ?, Gender = ?, Number_of_limbs = ?,
                Superpowers = ?, Biografia = ?';
        
        $stmt = $pdo->prepare($sql);
        
        $stmt->execute(array($_POST['Name'], $_POST['E_mail'], $_POST['Date'], $_POST['Gender'],
            $_POST['Number_of_limbs'], serialize($_POST['Superpowers']), $_POST['Biografia']));
    }
?>