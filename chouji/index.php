<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

        <title>Chouji</title>
    </head>
    <body>
        <div class="row justify-content-center">
            <div class="col-md-4">
            	<?php
                	if (!empty($_COOKIE['errors'])) {
                	    $errors = json_decode( $_COOKIE['errors'] );
                	    
                	    $strErrors = '';

                	    foreach ($errors as $key=>$value) {
                	        $strErrors .= "● " . $value . '<br>';
                	    }
                	    
                	    print('<div class="alert alert-danger">' . $strErrors . '</div>');
                	}
            	
                	if($_SERVER['QUERY_STRING'] == 'success') {
                	    print('<div class="alert alert-success">Success</div>');
                	}
            	?>
                <form action="form.php" method="POST">
                    <div class="form-group">
                        <label><strong>Name</strong></label>
                        <input type="text" class="form-control" name="Name" placeholder="name"
                            <?php
                                if ( !empty($_COOKIE['errors']) ) {
                                    print('value="' . json_decode($_COOKIE['wrong_info']) -> Name . '"');
                                    
                                    if ( array_key_exists('Name', json_decode($_COOKIE['errors']) ) ) {
                                        print 'style="border: 2px solid red;"';
                                    }
                                }
                                else if ( !empty($_COOKIE['success_info']) ) {
                                    print('value="' . json_decode($_COOKIE['success_info']) -> Name . '"');
                                }
                            ?>
                       	>
                    </div>
                    <div class="form-group">
                        <label><strong>Email</strong></label>
                        <input type="text" class="form-control" name="E_mail" placeholder="e-mail"
                            <?php
                                if ( !empty($_COOKIE['errors']) ) {
                                    print('value="' . json_decode($_COOKIE['wrong_info']) -> E_mail . '"');
                                    
                                    if ( array_key_exists('E_mail', json_decode($_COOKIE['errors']) ) ) {
                                        print 'style="border: 2px solid red;"';
                                    }
                                }
                                else if ( !empty($_COOKIE['success_info']) ) {
                                    print('value="' . json_decode($_COOKIE['success_info']) -> E_mail . '"');
                                }
                            ?>
                        >
                    </div>
                    <div class="form-group">
                        <label><strong>Date</strong></label>
                        <select class="form-control" name="Date">
                            <?php 
                                if ( !empty($_COOKIE['errors']) ) {
                                        $that = json_decode($_COOKIE['wrong_info']) -> Date;
                                }
                                else if ( !empty($_COOKIE['success_info']) ) {
                                    $that = json_decode($_COOKIE['success_info']) -> Date;
                                }
                                for ($i = 1998; $i <= 2002; ++$i) {
                                    print('<option'. (($that == $i) ? ' selected="selected"' : '') . ' value="' . $i . '">'
                                        . $i . '</option>');
                                }
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label><strong>Gender</strong></label><br>
                        <div>
                            <input type="radio" name="Gender" value="Male" checked>
                            <label>
                                Male
                            </label>
                        </div>
                        <div>
                            <input type="radio" name="Gender" value="Female"
                                <?php
                                    if ( !empty($_COOKIE['errors']) ) {
                                        print( json_decode($_COOKIE['wrong_info']) -> Gender == 'Female' ? 'checked' : '' );
                                    }
                                    else if ( !empty($_COOKIE['success_info']) ) {
                                        print( json_decode($_COOKIE['success_info']) -> Gender == 'Female' ? 'checked' : '' );
                                    }
                                ?>
                            >
                            <label>
                                Female
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label><strong>Number of limbs</strong></label><br>
                        <?php
                            $that = 0;
                            if ( !empty($_COOKIE['errors']) ) {
                                $that = json_decode($_COOKIE['wrong_info']) -> Number_of_limbs;
                            }
                            else if ( !empty($_COOKIE['success_info']) ) {
                                $that = json_decode($_COOKIE['success_info']) -> Number_of_limbs;
                            }
                            for ($i = 0; $i <= 4; ++$i) {
                                print('<input' . ($that == $i ? ' checked="checked"' : '')
                                    . ' type="radio" name="Number_of_limbs" value="' . $i . '">');
                                print('<label>' . $i . '</label>');
                            }
                        ?>
                    </div>
                    <div class="form-group">
                        <label><strong>Superpowers</strong></label><br>
                        <select multiple name="Superpowers[]" class="form-control"
                        <?php
                            if ( !empty($_COOKIE['errors']) ) {
                                if ( array_key_exists('Superpowers', json_decode($_COOKIE['errors']) ) ) {
                                    print 'style="border: 2px solid red;"';
                                }
                            }
                        ?>
                        >
                          <option value="Immortality"
                          <?php
                              if ( !empty($_COOKIE['errors']) ) {
                                  print( preg_match('/Immortality/', json_decode($_COOKIE['wrong_info']) -> Superpowers) ?
                                      ' selected="selected"' : '' );
                              }
                              else if ( !empty($_COOKIE['success_info']) ) {
                                  print( preg_match('/Immortality/', json_decode($_COOKIE['success_info']) -> Superpowers) ?
                                      ' selected="selected"' : '' );
                              }
                          ?>
                          >Immortality</option>
                          <option value="Passing through walls"
                          <?php
                               if ( !empty($_COOKIE['errors']) ) {
                                   print( preg_match('/Passing through walls/', json_decode($_COOKIE['wrong_info']) -> Superpowers) ?
                                       ' selected="selected"' : '' );
                               }
                               else if ( !empty($_COOKIE['success_info']) ) {
                                   print( preg_match('/Passing through walls/', json_decode($_COOKIE['success_info']) -> Superpowers) ?
                                       ' selected="selected"' : '' );
                               }
                          ?>
                          >Passing through walls</option>
                          <option value="Levitation"
                          <?php
                              if ( !empty($_COOKIE['errors']) ) {
                                  print( preg_match('/Levitation/', json_decode($_COOKIE['wrong_info']) -> Superpowers) ?
                                      ' selected="selected"' : '' );
                              }
                              else if ( !empty($_COOKIE['success_info']) ) {
                                  print( preg_match('/Levitation/', json_decode($_COOKIE['success_info']) -> Superpowers) ?
                                      ' selected="selected"' : '' );
                              }
                              ?>
                          >Levitation</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label><strong>Biografia</strong></label><br>
                        <textarea name="Biografia" class="form-control" placeholder="Enter text"
                            <?php
                                if ( !empty($_COOKIE['errors']) ) {
                                    if ( array_key_exists('Biografia', json_decode($_COOKIE['errors']) ) ) {
                                        print 'style="border: 2px solid red;"';
                                    }
                                }
                            ?>
                        ><?php if ( !empty($_COOKIE['errors']) ) { print( json_decode($_COOKIE['wrong_info']) -> Biografia ); } else if ( !empty($_COOKIE['success_info']) ) { print( json_decode($_COOKIE['success_info']) -> Biografia ); }?></textarea>
                    </div>
                    <div class="form-group">
                        <input type="checkbox" name="Accept"
                            <?php
                            if ( !empty($_COOKIE['errors']) ) {
                                if ( json_decode($_COOKIE['wrong_info']) -> Accept )
                                    print('checked="checked"');
                            }
                            else if ( !empty($_COOKIE['success_info']) ) {
                                print('checked="checked"');
                            }

                            ?>
                        >
                        <label                         
                            <?php
                                if ( !empty($_COOKIE['errors']) &&
                                    array_key_exists('Accept', json_decode($_COOKIE['errors']) ) )
                                print 'style="color: red;"';
                            ?>
                        >
                            Accept the privacy policy
                        </label>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div> 
    </body>
</html>
