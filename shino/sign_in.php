<?php
session_start();
if($_SERVER['QUERY_STRING'] == 'log_out') {
    session_destroy();
    
    header('Location: /shino/sign_in.php');
}
if ( !empty($_SESSION['login']) &&  !empty($_SESSION['password']) ) {
    if ( !empty($_POST) ) {
        $errors = getArrayErrors();
        
        if (count($errors) > 0) {
            setcookie('change_database_errors', json_encode($errors), time() + 24 * 60 * 60);
            
            setcookie('change_database_wrong_info', json_encode( array('Name' => $_POST['Name'],
                'E_mail' => $_POST['E_mail'],
                'Date' => $_POST['Date'],
                'Gender' => $_POST['Gender'],
                'Number_of_limbs' => $_POST['Number_of_limbs'],
                'Superpowers' => implode(', ', $_POST['Superpowers']),
                'Biografia' => $_POST['Biografia'],
                'Accept' => empty($_POST['Accept']) ? false : true )
                ), time() + 24 * 60 * 60);
         
            header('Location: http://u15643.kubsu-dev.ru/shino/sign_in.php');
            
            exit();
        } else {
            setcookie('change_database_errors', '', 1000);
            
            setcookie('change_database_wrong_info', '', 1000);
            
            $connection = 'mysql:host=localhost;dbname=u15643';
            $pdo = new PDO($connection, 'u15643', '2705239');
            
            //check security token
            if ( !empty($_POST['security_token']) ) {
                $sql = 'SELECT * FROM security_token';
                
                $stmt = $pdo->prepare($sql);
                $stmt->execute();
                
                $stmt->bindColumn(1, $Token);
                
                $flag = false;
                while ($row = $stmt->fetch(PDO::FETCH_BOUND)) {
                    if ( $_POST['security_token'] == $Token) {
                        $flag = true;
                    }
                }
                if (!$flag) {
                    print("Error");
                    exit();
                }
            } else {
                print("Error");
                exit();
            }
            
            $sql = 'UPDATE shino SET Name = ?, E_mail = ?, Date = ?, Gender = ?, Number_of_limbs = ?,
                Superpowers = ?, Biografia = ? WHERE Login = ? AND Password = ?'  ;
            
            $stmt = $pdo->prepare($sql);
            
            $stmt->execute( array($_POST['Name'], $_POST['E_mail'], $_POST['Date'], $_POST['Gender'],
                $_POST['Number_of_limbs'], serialize($_POST['Superpowers']), $_POST['Biografia'], $_SESSION['login'],  md5($_SESSION['password'])) );
            
            header('Location: ?success');
        }
        
    };
    
    
    $connection = 'mysql:host=localhost;dbname=u15643';
    $pdo = new PDO($connection, 'u15643', '2705239');
    
    $sql = 'SELECT * FROM shino WHERE Login ="' . $_SESSION['login'] . '" AND Password ="' . md5($_SESSION['password']) . '"';
    
    $stmt = $pdo->prepare($sql);
    $stmt->execute();
    
    $stmt->bindColumn(2, $Name);
    $stmt->bindColumn(3, $E_mail);
    $stmt->bindColumn(4, $Date);
    $stmt->bindColumn(5, $Gender);
    $stmt->bindColumn(6, $Number_of_limbs);
    $stmt->bindColumn(7, $Superpowers);
    $stmt->bindColumn(8, $Biografia);
    
    
    $stmt->fetch(PDO::FETCH_BOUND);
    
    // put the information in a variable that replaces the return value from the cookie
    $substitution_change_database_success_info = json_encode( array('Name' => $Name,
        'E_mail' => $E_mail,
        'Date' => $Date,
        'Gender' => $Gender,
        'Number_of_limbs' => $Number_of_limbs,
        'Superpowers' => implode(', ', unserialize($Superpowers)),
        'Biografia' => $Biografia,
        'Accept' => true )
    );
    
    // variables for change_database_page.php -> form_module.php
    $cookie_errors = !empty($_COOKIE['change_database_errors']) ? $_COOKIE['change_database_errors'] : null;
    $cookie_wrong_info = !empty($_COOKIE['change_database_wrong_info']) ? $_COOKIE['change_database_wrong_info'] : null;
    $cookie_success_info = $substitution_change_database_success_info;
    
    //security token
    $connection = 'mysql:host=localhost;dbname=u15643';
    $pdo = new PDO($connection, 'u15643', '2705239');
    
    $security_token =  md5( bin2hex(random_bytes(3)) );
    $sql = 'INSERT INTO security_token SET Token = ?';
    $stmt = $pdo->prepare($sql);
    $stmt->execute( array($security_token) );
    
    include 'change_database_page.php';
    
    exit();
    
} else if ( !empty($_POST['Login']) && !empty($_POST['Password']) ) {
    $connection = 'mysql:host=localhost;dbname=u15643';
    $pdo = new PDO($connection, 'u15643', '2705239');
    
    $sql = 'SELECT * FROM shino WHERE Login ="' . $_POST['Login'] . '" AND Password ="' . md5($_POST['Password']) . '"';
    
    $stmt = $pdo->prepare($sql);
    $stmt->execute();
    
    $stmt->bindColumn(2, $Name);
    $stmt->bindColumn(3, $E_mail);
    $stmt->bindColumn(4, $Date);
    $stmt->bindColumn(5, $Gender);
    $stmt->bindColumn(6, $Number_of_limbs);
    $stmt->bindColumn(7, $Superpowers);
    $stmt->bindColumn(8, $Biografia);
    
    
    $stmt->fetch(PDO::FETCH_BOUND);
    
    // put the information in a variable that replaces the return value from the cookie
    $substitution_change_database_success_info = json_encode( array('Name' => $Name,
        'E_mail' => $E_mail,
        'Date' => $Date,
        'Gender' => $Gender,
        'Number_of_limbs' => $Number_of_limbs,
        'Superpowers' => implode(', ', unserialize($Superpowers)),
        'Biografia' => $Biografia,
        'Accept' => true )
        );
    
    if( !empty($Name) ) {
        $_SESSION['login'] = $_POST['Login'];
        $_SESSION['password'] = $_POST['Password'];
        
        // variables for change_database_page.php -> form_module.php
        $cookie_errors = !empty($_COOKIE['change_database_errors']) ? $_COOKIE['change_database_errors'] : null;
        $cookie_wrong_info = !empty($_COOKIE['change_database_wrong_info']) ? $_COOKIE['change_database_wrong_info'] : null;
        $cookie_success_info = $substitution_change_database_success_info;
        
        //security token
        $connection = 'mysql:host=localhost;dbname=u15643';
        $pdo = new PDO($connection, 'u15643', '2705239');
        
        $security_token =  md5( bin2hex(random_bytes(3)) );
        $sql = 'INSERT INTO security_token SET Token = ?';
        $stmt = $pdo->prepare($sql);
        $stmt->execute( array($security_token) );
         
        include 'change_database_page.php';
        
        exit();
    } else {
        setcookie('wrong_sign_in', 'Invalid "Login" or "Password"');
        
        header('Location: http://u15643.kubsu-dev.ru/shino/sign_in.php');
    }
}


include 'sign_in_page.php';



function getArrayErrors() {
    $errors = array();
    
    if ( preg_match('/^$/', $_POST['Name']) ) {
        $errors['Name'] = 'Empty "Name" field';
    } else if (strlen($_POST['Name']) > 20) {
        $errors['Name'] = '"Name" is too long';
    } else if ( !preg_match('/^[a-zA-Z]{1}[a-zA-Z]*$/', $_POST['Name']) ) {
        $errors['Name'] = '"Name" field must consist only of letters';
    }
    
    if( preg_match('/^$/', $_POST['E_mail']) ) {
        $errors['E_mail'] = 'Empty "Email" field';
    } else if (strlen($_POST['E_mail']) > 320) {
        $errors['E_mail'] = '"Email" is too long';
    } else if ( !preg_match('/[^\s]+@[^\s\.]+\.[a-z]+/', $_POST['E_mail']) ) {
        $errors['E_mail'] = 'Invalid format for the "Email" field. Right format for example: example@mail.com';
    }
    
    if (empty($_POST['Superpowers'])) {
        $errors['Superpowers'] = '"Superpower" is not selected';
    }
    
    if ( preg_match('/^$/', $_POST['Biografia']) ) {
        $errors['Biografia'] = 'Empty "Biografia" field';
    } else if (strlen($_POST['Biografia']) > 320) {
        $errors['Biografia'] = '"Biografia" is too long';
    }
    
    if (empty($_POST['Accept'])) {
        $errors['Accept'] = 'The privacy policy has not been accepted';
    }
    
    return $errors;
}

function addInDB($login, $password) {
    $connection = 'mysql:host=localhost;dbname=u15643';
    $pdo = new PDO($connection, 'u15643', '2705239');
    
    $sql = 'INSERT INTO shino SET Name = ?, E_mail = ?, Date = ?, Gender = ?, Number_of_limbs = ?,
                Superpowers = ?, Biografia = ?, Login = ? , Password = ?';
    
    $stmt = $pdo->prepare($sql);
    
    $stmt->execute(array($_POST['Name'], $_POST['E_mail'], $_POST['Date'], $_POST['Gender'],
        $_POST['Number_of_limbs'], serialize($_POST['Superpowers']), $_POST['Biografia'], $login, md5($password)));
}
?>