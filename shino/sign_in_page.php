<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

        <title>Shino</title>
    </head>
    <body>
		<div>
    		<button class="btn btn-dark"
    			onclick="window.location.href = 'http://u15643.kubsu-dev.ru/shino/registration.php';">Registration</button>
    		<button class="btn btn-dark"
    			onclick="window.location.href = 'http://u15643.kubsu-dev.ru/shino/sign_in.php';">Sign in</button>
    	</div>

		        <?php 
		        if ( !empty($_COOKIE['login']) && !empty($_COOKIE['password'])) {
            	    print('<div class="alert alert-success">
                        Your login: ' . $_COOKIE['login'] . '<br>Your password: ' . $_COOKIE['password'] . '</div>');
            	    
            	    setcookie('login', '', 1000);
            	    setcookie('password', '', 1000);
            	}
            	
            	if ( !empty($_COOKIE['wrong_sign_in']) ) {
            	    print('<div class="alert alert-danger">' . $_COOKIE['wrong_sign_in'] . '</div>');
            	    
            	    setcookie('wrong_sign_in', '', 1000);
            	}
            	?> 
            	<h2>Sign in</h2>
                <form action="" method="POST">
                    <div class="form-group">
                        <label><strong>Login</strong></label>
                        <input type="text" class="form-control" name="Login" placeholder="Login">
                    </div>
                    <div class="form-group">
                        <label><strong>Password</strong></label>
                        <input type="text" class="form-control" name="Password" placeholder="Password">
                    </div>
                    <button type="submit" class="btn btn-dark">Sign in</button>
                </form>
        
    </body>
</html>