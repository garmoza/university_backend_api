<?php
if (!empty($_POST)) {
    $errors = getArrayErrors();
    
    if (count($errors) > 0) {
        setcookie('registration_errors', json_encode($errors), time() + 24 * 60 * 60);
    
        setcookie('registration_wrong_info', json_encode( array('Name' => $_POST['Name'],
            'E_mail' => $_POST['E_mail'],
            'Date' => $_POST['Date'],
            'Gender' => $_POST['Gender'],
            'Number_of_limbs' => $_POST['Number_of_limbs'],
            'Superpowers' => implode(', ', $_POST['Superpowers']),
            'Biografia' => $_POST['Biografia'],
            'Accept' => empty($_POST['Accept']) ? false : true )
            ), time() + 24 * 60 * 60);
    
        header('Location: http://u15643.kubsu-dev.ru/shino/registration.php');
    
        exit();
    } else {
        setcookie('registration_errors', '', 1000);
        
        setcookie('registration_wrong_info', '', 1000);
        
        setcookie('registration_success_info', json_encode( array('Name' => $_POST['Name'],
            'E_mail' => $_POST['E_mail'],
            'Date' => $_POST['Date'],
            'Gender' => $_POST['Gender'],
            'Number_of_limbs' => $_POST['Number_of_limbs'],
            'Superpowers' => implode(', ', $_POST['Superpowers']),
            'Biografia' => $_POST['Biografia'],
            'Accept' => empty($_POST['Accept']) ? false : true )
            ), time() + 24 * 60 * 60 * 365);
        
        $login = bin2hex(random_bytes(3));
        $password = bin2hex(random_bytes(3));
        
        setcookie('login', $login);
        setcookie('password', $password);
        
        addInDB($login, $password);
        
        header('Location: http://u15643.kubsu-dev.ru/shino/sign_in.php');
    }
    
};

// variables for regisration_page.php -> form_module.php
$cookie_errors = !empty($_COOKIE['registration_errors']) ? $_COOKIE['registration_errors'] : null;
$cookie_wrong_info = !empty($_COOKIE['registration_wrong_info']) ? $_COOKIE['registration_wrong_info'] : null;
$cookie_success_info = !empty($_COOKIE['registration_success_info']) ? $_COOKIE['registration_success_info'] : null;

include 'registration_page.php';



function getArrayErrors() {
    $errors = array();
    
    if ( preg_match('/^$/', $_POST['Name']) ) {
        $errors['Name'] = 'Empty "Name" field';
    } else if (strlen($_POST['Name']) > 20) {
        $errors['Name'] = '"Name" is too long';
    } else if ( !preg_match('/^[a-zA-Z]{1}[a-zA-Z]*$/', $_POST['Name']) ) {
        $errors['Name'] = '"Name" field must consist only of letters';
    }
    
    if( preg_match('/^$/', $_POST['E_mail']) ) {
        $errors['E_mail'] = 'Empty "Email" field';
    } else if (strlen($_POST['E_mail']) > 320) {
        $errors['E_mail'] = '"Email" is too long';
    } else if ( !preg_match('/[^\s]+@[^\s\.]+\.[a-z]+/', $_POST['E_mail']) ) {
        $errors['E_mail'] = 'Invalid format for the "Email" field. Right format for example: example@mail.com';
    }

    if (empty($_POST['Superpowers'])) {
        $errors['Superpowers'] = '"Superpower" is not selected';
    }
    
    if ( preg_match('/^$/', $_POST['Biografia']) ) {
        $errors['Biografia'] = 'Empty "Biografia" field';
    } else if (strlen($_POST['Biografia']) > 320) {
        $errors['Biografia'] = '"Biografia" is too long';
    } else if ( preg_match('/[<>]/', $_POST['Biografia'])) {
        $errors['Biografia'] = 'In the "Biografia" field you can`t use <,>';
    }
    
    if (empty($_POST['Accept'])) {
        $errors['Accept'] = 'The privacy policy has not been accepted';
    }
    
    return $errors;
}

function addInDB($login, $password) {
    $connection = 'mysql:host=localhost;dbname=u15643';
    $pdo = new PDO($connection, 'u15643', '2705239');
    
    $sql = 'INSERT INTO shino SET Name = ?, E_mail = ?, Date = ?, Gender = ?, Number_of_limbs = ?,
                Superpowers = ?, Biografia = ?, Login = ? , Password = ?';
    
    $stmt = $pdo->prepare($sql);
    
    $stmt->execute(array($_POST['Name'], $_POST['E_mail'], $_POST['Date'], $_POST['Gender'],
        $_POST['Number_of_limbs'], serialize($_POST['Superpowers']), $_POST['Biografia'], $login, md5($password)));
}
?>