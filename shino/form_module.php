<?php
if ($cookie_errors != null) {
    $errors = json_decode($cookie_errors);
    
    $strErrors = '';

    foreach ($errors as $key=>$value) {
        $strErrors .= "● " . $value . '<br>';
    }
    
    print('<div class="alert alert-danger">' . $strErrors . '</div>');
}

if($_SERVER['QUERY_STRING'] == 'success') {
    print('<div class="alert alert-success">Success</div>');
}
?>
<form action="" method="POST">
	<input name="security_token" type="hidden" value="<?php if(!empty($security_token)) print($security_token)?>"/>
    <div class="form-group">
        <label><strong>Name</strong></label>
        <input type="text" class="form-control" name="Name" placeholder="name"
            <?php
            if ($cookie_errors != null) {
                print('value="' . json_decode($cookie_wrong_info) -> Name . '"');
                
                if ( array_key_exists('Name', json_decode($cookie_errors) ) ) {
                    print 'style="border: 2px solid red;"';
                }
            }
            else if ($cookie_success_info != null) {
                print('value="' . json_decode($cookie_success_info) -> Name . '"');
            }
            ?>
       	>
    </div>
    <div class="form-group">
        <label><strong>Email</strong></label>
        <input type="text" class="form-control" name="E_mail" placeholder="e-mail"
            <?php
            if ($cookie_errors != null) {
                print('value="' . json_decode($cookie_wrong_info) -> E_mail . '"');
                
                if ( array_key_exists('E_mail', json_decode($cookie_errors) ) ) {
                    print 'style="border: 2px solid red;"';
                }
            }
            else if ($cookie_success_info != null) {
                print('value="' . json_decode($cookie_success_info) -> E_mail . '"');
            }
            ?>
        >
    </div>
    <div class="form-group">
        <label><strong>Date</strong></label>
        <select class="form-control" name="Date">
            <?php 
            if ($cookie_errors != null) {
                $that = json_decode($cookie_wrong_info) -> Date;
            }
            else if ($cookie_success_info != null) {
                $that = json_decode($cookie_success_info) -> Date;
            }
            for ($i = 1998; $i <= 2002; ++$i) {
                print('<option'. (($that == $i) ? ' selected="selected"' : '') . ' value="' . $i . '">'
                    . $i . '</option>');
            }
            ?>
        </select>
    </div>
    <div class="form-group">
        <label><strong>Gender</strong></label><br>
        <div>
            <input type="radio" name="Gender" value="Male" checked>
            <label>
                Male
            </label>
        </div>
        <div>
            <input type="radio" name="Gender" value="Female"
                <?php
                if ($cookie_errors != null) {
                    print( json_decode($cookie_wrong_info) -> Gender == 'Female' ? 'checked' : '' );
                }
                else if ($cookie_success_info != null) {
                    print( json_decode($cookie_success_info) -> Gender == 'Female' ? 'checked' : '' );
                }
                ?>
            >
            <label>
                Female
            </label>
        </div>
    </div>
    <div class="form-group">
        <label><strong>Number of limbs</strong></label><br>
        <?php
            $that = 0;
            if ($cookie_errors != null) {
                $that = json_decode($cookie_wrong_info) -> Number_of_limbs;
            }
            else if ($cookie_success_info != null) {
                $that = json_decode($cookie_success_info) -> Number_of_limbs;
            }
            for ($i = 0; $i <= 4; ++$i) {
                print('<input' . ($that == $i ? ' checked="checked"' : '')
                    . ' type="radio" name="Number_of_limbs" value="' . $i . '">');
                print('<label>' . $i . '</label>');
            }
        ?>
    </div>
    <div class="form-group">
        <label><strong>Superpowers</strong></label><br>
        <select multiple name="Superpowers[]" class="form-control"
        <?php
        if ($cookie_errors != null) {
            if ( array_key_exists('Superpowers', json_decode($cookie_errors) ) ) {
                print 'style="border: 2px solid red;"';
            }
        }
        ?>
        >
          <option value="Immortality"
          <?php
          if ($cookie_errors != null) {
              print( preg_match('/Immortality/', json_decode($cookie_wrong_info) -> Superpowers) ?
                  ' selected="selected"' : '' );
          }
          else if ($cookie_success_info != null) {
              print( preg_match('/Immortality/', json_decode($cookie_success_info) -> Superpowers) ?
                  ' selected="selected"' : '' );
          }
          ?>
          >Immortality</option>
          <option value="Passing through walls"
          <?php
          if ($cookie_errors != null) {
              print( preg_match('/Passing through walls/', json_decode($cookie_wrong_info) -> Superpowers) ?
                   ' selected="selected"' : '' );
           }
           else if ($cookie_success_info != null) {
               print( preg_match('/Passing through walls/', json_decode($cookie_success_info) -> Superpowers) ?
                   ' selected="selected"' : '' );
           }
          ?>
          >Passing through walls</option>
          <option value="Levitation"
          <?php
          if ($cookie_errors != null) {
              print( preg_match('/Levitation/', json_decode($cookie_wrong_info) -> Superpowers) ?
                  ' selected="selected"' : '' );
          }
          else if ($cookie_success_info != null) {
              print( preg_match('/Levitation/', json_decode($cookie_success_info) -> Superpowers) ?
                  ' selected="selected"' : '' );
          }
          ?>
          >Levitation</option>
        </select>
    </div>
    <div class="form-group">
        <label><strong>Biografia</strong></label><br>
        <textarea name="Biografia" class="form-control" placeholder="Enter text"
            <?php
            if ($cookie_errors != null) {
                if ( array_key_exists('Biografia', json_decode($cookie_errors) ) ) {
                    print 'style="border: 2px solid red;"';
                }
            }
            ?>
        ><?php if ($cookie_errors != null) { print( json_decode($cookie_wrong_info) -> Biografia ); } else if ($cookie_success_info != null) { print( json_decode($cookie_success_info) -> Biografia ); }?></textarea>
    </div>
    <div class="form-group">
        <input type="checkbox" name="Accept"
            <?php
            if ($cookie_errors != null) {
                if ( json_decode($cookie_wrong_info) -> Accept )
                    print('checked="checked"');
            }
            else if ($cookie_success_info != null) {
                print('checked="checked"');
            }

            ?>
        >
        <label                         
            <?php
            if ($cookie_errors != null &&
                array_key_exists('Accept', json_decode($cookie_errors) ) )
            print 'style="color: red;"';
            ?>
        >
            Accept the privacy policy
        </label>
    </div>
    <button type="submit" class="btn btn-dark">Submit</button>
</form>