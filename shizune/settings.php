<?php
define('DISPLAY_ERRORS', 1);

define('INCLUDE_PATH', './scripts' . PATH_SEPARATOR . './modules');


$urlconf = array(
    '' => array('tpl' => 'default', 'module' => array('header_block')),
    '/^profile\/(\w+)/' => array('tpl' => 'default', 'module' => array('header_block', 'form'), 'auth' => 'profile_auth'),
    '/^registration$/' => array('tpl' => 'default', 'module' => array('header_block', 'form')),
    '/^submit_form$/' => array('module' => array('submit_form')),
    '/^admin$/' => array('tpl' => 'admin'),
    '/^sign_in$/' => array('tpl' => 'default', 'module' => array('header_block', 'sign_in'),  'auth' => 'profile_auth'),
);


header('Cache-Control: no-cache, must-revalidate');
header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
header('Content-Type: text/html; charset=UTF-32');
?>