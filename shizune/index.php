<?php
include('./settings.php');

ini_set('display_errors', DISPLAY_ERRORS);
error_reporting(E_ALL & E_STRICT);
ini_set("mysql.trace_mode","On");


ini_set('include_path', INCLUDE_PATH);


include('init.php');
include('db.php');


$request = array(
    'url' => isset($_GET['q']) ? $_GET['q'] : '',
    'method' => isset($_POST['method']) && in_array($_POST['method'], array('GET', 'POST', 'PUT', 'DELETE')) ? $_POST['method'] : $_SERVER['REQUEST_METHOD'],
    'accept' => $_SERVER['HTTP_ACCEPT'],
    'raw_request' => file_get_contents('php://input'),
);

$response = init($request, $urlconf);

foreach ($response['headers'] as $key => $value) {
    if (is_string($key)) {
        header(sprintf('%s: %s', $key, $value));
    } else {
        header($value);
    }
}

print($response['entity']);
?>