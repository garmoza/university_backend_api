<?php
if ( empty($_SERVER['PHP_AUTH_USER']) || empty($_SERVER['PHP_AUTH_PW']) ) {
    header('HTTP/1.1 401 Unanthorized');
    header('WWW-Authenticate: Basic realm="Shino"');
    print('<h1>401 Authorization required</h1>');
    exit();
}

$connection = 'mysql:host=localhost;dbname=u15643';
$pdo = new PDO($connection, 'u15643', '2705239');

$sql = 'SELECT * FROM admins';

$stmt = $pdo->prepare($sql);
$stmt->execute();

$stmt->bindColumn(1, $log);
$stmt->bindColumn(2, $pass);

$flag = false;
while ($row = $stmt->fetch(PDO::FETCH_BOUND)) {
    if ( $_SERVER['PHP_AUTH_USER'] == $log && md5($_SERVER['PHP_AUTH_PW']) == $pass) {
        $flag = true;
    }
}

if (!$flag) {
    header('HTTP/1.1 401 Unanthorized');
    header('WWW-Authenticate: Basic realm="Pakkun"');
    print('<h1>401 Authorization required</h1>');
    exit();
}

//delete data
if ( !empty($_GET['del']) ) {
    $sql = 'DELETE FROM shino WHERE Login = ?';
    
    $stmt = $pdo->prepare($sql);
    
    $stmt->execute(array($_GET['del']));
}

$sql = 'SELECT * FROM shino';

$stmt = $pdo->prepare($sql);
$stmt->execute();

$stmt->bindColumn(2, $Name);
$stmt->bindColumn(3, $E_mail);
$stmt->bindColumn(4, $Date);
$stmt->bindColumn(5, $Gender);
$stmt->bindColumn(6, $Number_of_limbs);
$stmt->bindColumn(7, $Superpowers);
$stmt->bindColumn(8, $Biografia);
$stmt->bindColumn(9, $Login);

echo '<table>';
echo '<tr style="font-weight:bold"><td>Name</td><td>E_mail</td><td>Date</td><td>Gender</td><td>Number_of_limbs</td>
              <td>Superpowers</td><td>Biografia</td><td>Login</td></tr>';
while ($row = $stmt->fetch(PDO::FETCH_BOUND)) {
    echo '<tr>';
    echo "<td>$Name</td>
              <td>$E_mail</td>
              <td>$Date</td>
              <td>$Gender</td>
              <td>$Number_of_limbs</td>
              <td>$Superpowers</td>
              <td>$Biografia</td>
              <td>$Login</td>
              <td><form method='GET'>
                <input type='hidden' name='del' value='$Login'>
                <button type='submit'>&#10008</button>
              </form></td>";
    echo '</tr>';
}

echo '</table>';
?>