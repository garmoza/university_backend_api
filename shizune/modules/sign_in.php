<?php
function sign_in_get($request) {
    $content = "";
    
    if ($_GET['rejection']) {
        $content .= <<<HTML
            <div class="alert alert-danger">Wrong error or password</div>
        HTML;
    }
    
    $content .= file_get_contents('./html/sign_in.html');
    
    return $content;
}
?>