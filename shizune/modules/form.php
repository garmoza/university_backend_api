<?php
function form_get($request) {
    
    if ( isset($request['authData']) ) {
        $authData = $request['authData'];
        
        $content = fillFromAuthData(file_get_contents('./html/form.html'), $authData);
        
        if ( !isset($request['authData']['authorized']) ) {
            $content .= <<<HTML
            <style>
            .form-control { background-color: #e0e0e0; }
            #btn_submit, #accept_submit { display: none; }
            </style>
            HTML;
        }
        
        return $content;
    }
    
    return fillDefault(file_get_contents('./html/form.html'));
}

function fillDefault($formHTML){
    return sprintf($formHTML,
        'POST',
        '',
        '',
        'selected="selected"',
        '',
        '',
        '',
        '',
        'checked',
        '',
        'checked',
        '',
        '',
        '',
        '',
        'selected="selected"',
        '',
        '',
        '',
        );
}

function fillFromAuthData($formHTML, $authData) {
    return sprintf($formHTML,
        'PUT',
        $authData['Name'],
        $authData['Email'],
        $authData['Date'] == '2002' ? 'selected="selected"' : '',
        $authData['Date'] == '2001' ? 'selected="selected"' : '',
        $authData['Date'] == '2000' ? 'selected="selected"' : '',
        $authData['Date'] == '1999' ? 'selected="selected"' : '',
        $authData['Date'] == '1998' ? 'selected="selected"' : '',
        $authData['Gender'] == 'Male' ? 'checked' : '',
        $authData['Gender'] == 'Female' ? 'checked' : '',
        $authData['Number_of_limbs'] == '4' ? 'checked' : '',
        $authData['Number_of_limbs'] == '3' ? 'checked' : '',
        $authData['Number_of_limbs'] == '2' ? 'checked' : '',
        $authData['Number_of_limbs'] == '1' ? 'checked' : '',
        $authData['Number_of_limbs'] == '0' ? 'checked' : '',
        preg_match('/Immortality/', $authData['Superpowers']) ? 'selected="selected"' : '',
        preg_match('/Passing through walls/', $authData['Superpowers']) ? 'selected="selected"' : '',
        preg_match('/Levitation/', $authData['Superpowers']) ? 'selected="selected"' : '',
        $authData['Biografia'],
        );
}
?>