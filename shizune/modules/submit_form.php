<?php
function submit_form_post($request) {
    $userData = array();
    
    foreach (json_decode($request['raw_request']) as $key => $value) {
        $userData[$key] = $value;
    }
    
    if ( empty($userData) ) {
        $userData = array('Name' => $_POST['Name'],
            'E_mail' => $_POST['E_mail'],
            'Date' => $_POST['Date'],
            'Gender' => $_POST['Gender'],
            'Number_of_limbs' => $_POST['Number_of_limbs'],
            'Superpowers' => $_POST['Superpowers'],
            'Biografia' => $_POST['Biografia'],
            'Accept' => $_POST['Accept'],
        );
    }
    
    $errors = getArrayErrors($userData);
    
    if (count($errors) > 0) {
        $response['headers'] = array('Content-Type' => 'text/html',
            'HTTP/1.1 421 Field validation');
        $response['entity'] = json_encode($errors);
        
        return $response;
    }
    
    $login = bin2hex(random_bytes(3));
    $password = bin2hex(random_bytes(3));
    
    // ��������� ���� Accept �� �������
    array_pop($userData);
    $userData['login'] = $login;
    $userData['password'] = md5($password);
    $userData['Superpowers'] = serialize($userData['Superpowers']);
    
    insertDB('shino', array_values($userData));
    
    return json_encode( array('login' => $login,
                              'password' => $password,
                              'profile' => 'http://' . $_SERVER['HTTP_HOST'] . '/profile/' . $login) );
}

function submit_form_put($request) {
    $userData = array();
    
    foreach (json_decode($request['raw_request']) as $key => $value) {
        $userData[$key] = $value;
    }
    
    if ( empty($userData) ) {
        $userData = array('Name' => $_POST['Name'],
            'E_mail' => $_POST['E_mail'],
            'Date' => $_POST['Date'],
            'Gender' => $_POST['Gender'],
            'Number_of_limbs' => $_POST['Number_of_limbs'],
            'Superpowers' => $_POST['Superpowers'],
            'Biografia' => $_POST['Biografia'],
            'Accept' => $_POST['Accept'],
        );
    }
    
    $errors = getArrayErrors($userData);
    
    if (count($errors) > 0) {
        $response['headers'] = array('Content-Type' => 'text/html',
            'HTTP/1.1 421 Field validation');
        $response['entity'] = json_encode($errors);
        
        return $response;
    }
    
    array_pop($userData);
    $userData['Superpowers'] = serialize($userData['Superpowers']);
    session_start();
    $userData['login'] = $_SESSION['login'];
    $userData['password'] = md5($_SESSION['password']);
    
    updateDB('shino', array_values($userData));
    
    return json_encode( array('Info' => 'Data was changed successfully') );
}

function getArrayErrors($userData) {
    $errors = array();
    
    if ( preg_match('/^$/', $userData['Name']) ) {
        $errors['Name'] = 'Empty "Name" field';
    } else if (strlen($userData['Name']) > 20) {
        $errors['Name'] = '"Name" is too long';
    } else if ( !preg_match('/^[a-zA-Z]{1}[a-zA-Z]*$/', $userData['Name']) ) {
        $errors['Name'] = '"Name" field must consist only of letters';
    }
    
    if( preg_match('/^$/', $userData['E_mail']) ) {
        $errors['E_mail'] = 'Empty "Email" field';
    } else if (strlen($userData['E_mail']) > 320) {
        $errors['E_mail'] = '"Email" is too long';
    } else if ( !preg_match('/[^\s]+@[^\s\.]+\.[a-z]+/', $userData['E_mail']) ) {
        $errors['E_mail'] = 'Invalid format for the "Email" field. Right format for example: example@mail.com';
    }
    
    if (empty($userData['Superpowers'])) {
        $errors['Superpowers'] = '"Superpower" is not selected';
    }
    
    if ( preg_match('/^$/', $userData['Biografia']) ) {
        $errors['Biografia'] = 'Empty "Biografia" field';
    } else if (strlen($userData['Biografia']) > 320) {
        $errors['Biografia'] = '"Biografia" is too long';
    } else if ( preg_match('/[<>]/', $userData['Biografia'])) {
        $errors['Biografia'] = 'In the "Biografia" field you can`t use <,>';
    }
    
    if (!$userData['Accept']) {
        $errors['Accept'] = 'The privacy policy has not been accepted';
    }
    
    return $errors;
}
?>