<?php
function init($request, $urlconf) {
    $q = $request['url'];
    $method = $request['method'];
  
    foreach ($urlconf as $url => $properties) {
        if ($url == '' || $url[0] != '/') {
            if ($url != $q) {
                continue;
            }
        } else {
            if (!preg_match_all($url, $q, $matches)) {
                continue;
            }
        }
    
        if (isset($properties['auth'])) {
            require_once($properties['auth'] . '.php');
            
            $authData = auth($request);
            
            if (isset($authData['headers'])) {
                return $authData;
            }
            
            $request['authData'] = $authData;
        }
        
        $components = array();
        if (isset($properties['module'])) {
            foreach ($properties['module'] as $module) {
                require_once($module . '.php');
                $func = sprintf('%s_%s', $module, $method);
                if (!function_exists($func)) {
                    continue;
                }
                
                $component = call_user_func_array($func, array($request));
                
                if (isset($component['headers'])) {
                    return $component;
                }
                
                array_push($components, $component);
            } 
        }

        return builder($properties['tpl'], $components);
    }
}

function builder($template, $components) {
    $template = './theme/' . str_replace('/', '_', $template) . '.tpl.php';
    
    if (!file_exists($template)) {
        $response['headers'] = array('Content-Type' => 'text/json');
        $response['entity'] = implode('', $components);;
        return $response;
    }
    
    ob_start();
    include $template;
    $contents = ob_get_contents();
    ob_end_clean();
    
    $response['headers'] = array('Content-Type' => 'text/html');
    $response['entity'] = $contents;
    return $response;
}

function redirect($location) {
    return array('headers' => array('Location' => 'http://'. $_SERVER['HTTP_HOST'] . '/' . $location));
}

function responseNotFound() {
    $response['headers'] = array('Content-Type' => 'text/html',
                                 'HTTP/1.1 404 Not found');
    $response['entity'] = file_get_contents('./errors/404.tpl.php');
    
    return $response;
}
?>