<?php
function auth($request) {
    session_start();

    if ($_GET['log_out']) {
        session_destroy();
        return redirect('sign_in');
    }
    
    $connection = 'mysql:host=localhost;dbname=u15643';
    $pdo = new PDO($connection, 'u15643', '2705239');

    if ($request['url'] != 'sign_in') {
        $userLogin = preg_split("/[\/]/", $request['url'])[1];
        
        $sql = 'SELECT * FROM shino WHERE Login = ?';
        
        $stmt = $pdo->prepare($sql);
        $stmt->execute(array($userLogin));
        
        $stmt->bindColumn(2, $Name);
        $stmt->bindColumn(3, $E_mail);
        $stmt->bindColumn(4, $Date);
        $stmt->bindColumn(5, $Gender);
        $stmt->bindColumn(6, $Number_of_limbs);
        $stmt->bindColumn(7, $Superpowers);
        $stmt->bindColumn(8, $Biografia);
        $stmt->bindColumn(9, $login);
        $stmt->bindColumn(10, $password);
        
        $stmt->fetch(PDO::FETCH_BOUND);
        
        if( !empty($Name) ) {
            $data = array('Name' => $Name, 'Email' => $E_mail, 'Date' => $Date, 'Gender' => $Gender,
                'Number_of_limbs' => $Number_of_limbs, 'Superpowers' => $Superpowers, 'Biografia' => $Biografia);
        } else {
            return responseNotFound();
        }
        
        if ( !empty($_SESSION['login']) &&  !empty($_SESSION['password']) &&
            $_SESSION['login'] == $login && md5($_SESSION['password']) == $password ) {
                $data['authorized'] = True;
        }
        
        return $data;
    }
    
    if ($request['method'] == 'POST') {
        if ( !empty($_POST['Login']) && !empty($_POST['Password']) ) {      
            $sql = 'SELECT * FROM shino WHERE Login ="' . $_POST['Login'] . '" AND Password ="' . md5($_POST['Password']) . '"';
            
            $stmt = $pdo->prepare($sql);
            $stmt->execute();
            
            $stmt->bindColumn(2, $Name);
            $stmt->bindColumn(3, $E_mail);
            $stmt->bindColumn(4, $Date);
            $stmt->bindColumn(5, $Gender);
            $stmt->bindColumn(6, $Number_of_limbs);
            $stmt->bindColumn(7, $Superpowers);
            $stmt->bindColumn(8, $Biografia);
            
            
            $stmt->fetch(PDO::FETCH_BOUND);
            
            if( !empty($Name) ) {
                $_SESSION['login'] = $_POST['Login'];
                $_SESSION['password'] = $_POST['Password'];
                
                return redirect('profile/' . $_SESSION['login']);;
            }
            
            return redirect('sign_in?rejection=1');
        }
        
        return redirect('sign_in?rejection=1');
    }
    
    if ($request['method'] == 'GET') {
        if ( !empty($_SESSION['login']) && !empty($_SESSION['password']) ) {
            $sql = 'SELECT * FROM shino WHERE Login ="' . $_SESSION['login'] . '" AND Password ="' . md5($_SESSION['password']) . '"';

            $stmt = $pdo->prepare($sql);
            $stmt->execute();

            $stmt->bindColumn(2, $Name);
            $stmt->bindColumn(3, $E_mail);
            $stmt->bindColumn(4, $Date);
            $stmt->bindColumn(5, $Gender);
            $stmt->bindColumn(6, $Number_of_limbs);
            $stmt->bindColumn(7, $Superpowers);
            $stmt->bindColumn(8, $Biografia);


            $stmt->fetch(PDO::FETCH_BOUND);

            if( !empty($Name) ) {
                return redirect('profile/' . $_SESSION['login']);
            }
        }
    }
}
?>