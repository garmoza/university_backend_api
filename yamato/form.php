<?php 
    $errors = array();
    
    if(empty($_POST['Name'])) {
        $errors[] = '● Empty "Name" field';
    }

    if(empty($_POST['E_mail'])) {
        $errors[] = '● Empty "Email" field';
    }

    if(empty($_POST['Biografia'])) {
        $errors[] = '● Empty "Biografia" field';
    }

    if(empty($_POST['Accept'])) {
        $errors[] = '● The privacy policy has not been accepted';
    }

    $strErrors = '';
    if(count($errors)>0) {
        $strErrors = $errors[0];
    }
    for ($i = 1; $i < count($errors); $i++) {
        $strErrors .= '<br>' . $errors[$i];
    }

    if(count($errors) == 0) {
        $connection = 'mysql:host=localhost;dbname=u15643';
        $pdo = new PDO($connection, 'u15643', '2705239');
    
        $sql = 'INSERT INTO application(Name, E_mail, Date, Gender, Number_of_limbs, Superpowers, Biografia)
        VALUES(:Name, :E_mail, :Date, :Gender, :Number_of_limbs, :Superpowers, :Biografia)';
        $query = $pdo->prepare($sql);
        $query->execute(['Name'=>$_POST['Name'],'E_mail'=>$_POST['E_mail'], 'Date'=>$_POST['Date'], 'Gender'=>$_POST['Gender'],
        'Number_of_limbs'=>$_POST['Number_of_limbs'], 'Superpowers'=>$_POST['Superpowers'], 'Biografia'=>$_POST['Biografia']]);

        $strErrors .= "Success!";
    }
?>
<script>
    localStorage.setItem('errors', '<?=$strErrors; ?>');
</script>
<?php include('index.php'); ?>