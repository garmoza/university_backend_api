<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link rel="stylesheet" href="style.css">

        <script src="localStorage.js" defer></script>

        <title>Yamato</title>
    </head>
    <body>
        <div class="row justify-content-center">
            <div class="col-md-4">
                <div class="alert alert-danger" id="lableErrors" style="display: none;"></div>
                <div class="alert alert-success" id="lableSuccess" style="display: none;"></div>
                <script>
                    var errors = localStorage.getItem('errors');
                    if (errors != null) {
                        if(errors == 'Success!') {
                            document.getElementById("lableSuccess").innerHTML = "Success!";
                            document.getElementById("lableSuccess").style = "display: block;";
                        }
                        else {
                            document.getElementById("lableErrors").innerHTML = errors;
                            document.getElementById("lableErrors").style = "display: block;";
                        }
                    }
                    localStorage.removeItem('errors');
                </script>
                <form id="yoForm" action="form.php" method="post">
                    <div class="form-group">
                        <label><strong>Name</strong></label>
                        <input type="text" class="form-control" name="Name" placeholder="name">
                    </div>
                    <div class="form-group">
                        <label><strong>Email</strong></label>
                        <input type="text" class="form-control" name="E_mail" placeholder="e-mail">
                    </div>
                    <div class="form-group">
                        <label><strong>Date</strong></label>
                        <select class="form-control" name="Date">
                            <option value="2002">2002</option>
                            <option value="2001">2001</option>
                            <option value="2000">2000</option>
                            <option value="1999">1999</option>
                            <option value="1998">1998</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label><strong>Gender</strong></label><br>
                        <div>
                            <input type="radio" name="Gender" value="Male" checked>
                            <label>
                                Male
                            </label>
                        </div>
                        <div>
                            <input type="radio" name="Gender" value="Female">
                            <label>
                                Female
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label><strong>Number of limbs</strong></label><br>
                        <input type="radio" name="Number_of_limbs" value="4" checked="checked">
                        <label>4</label>
                        <input type="radio" name="Number_of_limbs" value="3">
                        <label>3</label>
                        <input type="radio" name="Number_of_limbs" value="2">
                        <label>2</label>
                        <input type="radio" name="Number_of_limbs" value="1">
                        <label>1</label>
                        <input type="radio" name="Number_of_limbs" value="0">
                        <label>0</label>
                    </div>
                    <div class="form-group">
                        <label><strong>Superpowers</strong></label><br>
                        <select multiple name="Superpowers" class="form-control">
                          <option value="Immortality" selected="selected">Immortality</option>
                          <option value="Passing through walls">Passing through walls</option>
                          <option value="Levitation">Levitation</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label><strong>Biografia</strong></label><br>
                        <textarea name="Biografia" class="form-control" placeholder="Enter text"></textarea>
                    </div>
                    <div class="form-group">
                        <input type="checkbox" name="Accept">
                        <label>
                            Accept the privacy policy
                        </label>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div> 
    </body>
</html>