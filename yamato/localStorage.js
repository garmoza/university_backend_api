document.getElementsByName("Name")[0].value = localStorage.getItem("Name");
document.getElementsByName("E_mail")[0].value = localStorage.getItem("E_mail");
if(localStorage.getItem("Date") == null) {
    document.getElementsByName("Date")[0].value = "2002";
}
else {
    document.getElementsByName("Date")[0].value = localStorage.getItem("Date");
}
document.getElementsByName("Gender").forEach(function(item) {
    if(item.value == localStorage.getItem("Gender")) {
        item.checked = true;
    }
});
document.getElementsByName("Number_of_limbs").forEach(function(item) {
    if(item.value == localStorage.getItem("Number_of_limbs")) {
        item.checked = true;
    }
});
if (localStorage.getItem("Superpowers") != null) {
    for (let i=0; i<3; i++) { 
        document.getElementsByName("Superpowers")[0][i].selected = false;
    }
    var spowers = localStorage.getItem("Superpowers").split(",");
    for (let i=0; i<3; i++) {
        for (let j=0; j<spowers.length; j++) {
            if (document.getElementsByName("Superpowers")[0][i].value == spowers[j]) {
                document.getElementsByName("Superpowers")[0][i].selected = true;
            }
        }
    }
}
document.getElementsByName("Biografia")[0].value = localStorage.getItem("Biografia");


window.addEventListener("DOMContentLoaded", function () {
    document.getElementById("yoForm").addEventListener("input", function () {
        localStorage.setItem("Name", document.getElementsByName("Name")[0].value);
        localStorage.setItem("E_mail", document.getElementsByName("E_mail")[0].value);
        localStorage.setItem("Date", document.getElementsByName("Date")[0].value);
        document.getElementsByName("Gender").forEach(function(item) {
            if (item.checked == true) {
                localStorage.setItem("Gender", item.value);
            }
        });
        document.getElementsByName("Number_of_limbs").forEach(function(item) {
            if (item.checked == true) {
                localStorage.setItem("Number_of_limbs", item.value);
            }
        });
        var spowers = [];
        for (let i=0; i<3; i++) {
            if (document.getElementsByName("Superpowers")[0][i].selected) {
                spowers.push(document.getElementsByName("Superpowers")[0][i].value);
            }
        }
        localStorage.setItem("Superpowers", spowers);
        localStorage.setItem("Biografia", document.getElementsByName("Biografia")[0].value);
    });
});
