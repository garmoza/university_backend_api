<?php
    header('Content-Type: text/html; charset=UTF-8');    
    
    // The last lines of code from this file: header('Location: ?success');
    // change the URL, but also re-execute the code of the current file:
    // form.php. This is why the URL is checked for changes,
    // because the form fields become empty after the page is reloaded and
    // errors are displayed
    if($_SERVER['QUERY_STRING'] == 'success') {
        print('Success!');
        
        exit();
    }

    $errors = getArrayErrors();
    
    if (count($errors) > 0) {
        printErrors($errors);
        
        exit();
    }  
    
    addInDB();
    
    // Changing the URL so that the user doesn't send the form again      
    header('Location: ?success');
    
    ////////////////////////////////////////////////////////////
    
    function getArrayErrors() {
        $errors = array();
        
        if(empty($_POST['Name'])) {
            $errors[] = 'Empty "Name" field';
        } else if (strlen($_POST['Name']) > 20) {
            $errors[] = '"Name" is too long';
        }
        
        if(empty($_POST['E_mail'])) {
            $errors[] = 'Empty "Email" field';
        } else if (strlen($_POST['E_mail']) > 320) {
            $errors[] = '"Email" is too long';
        } else if ( !strpos($_POST['E_mail'], '@') || 0 == strpos($_POST['E_mail'], '@')
            || strlen($_POST['E_mail'])-1 == strpos($_POST['E_mail'], '@') ) {
            $errors[] = 'Invalid format for the "Email" field';
        }
        
        if(empty($_POST['Biografia'])) {
            $errors[] = 'Empty "Biografia" field';
        } else if (strlen($_POST['Biografia']) > 320) {
            $errors[] = '"Biografia" is too long';
        }
        
        if(empty($_POST['Accept'])) {
            $errors[] = 'The privacy policy has not been accepted';
        }
        
        return $errors;
    }
    
    function printErrors($errors) {
        $strErrors = $errors[0];
        
        for ($i = 1; $i < count($errors); $i++) {
            $strErrors .= '<br>' . $errors[$i];
        }
        
        print($strErrors);
    }
    
    function addInDB() {
        $connection = 'mysql:host=localhost;dbname=u15643';
        $pdo = new PDO($connection, 'u15643', '2705239');
        
        $sql = 'INSERT INTO temari SET Name = ?, E_mail = ?, Date = ?, Gender = ?, Number_of_limbs = ?,
                Superpowers = ?, Biografia = ?';
        
        $stmt = $pdo->prepare($sql);
        
        $stmt->execute(array($_POST['Name'], $_POST['E_mail'], $_POST['Date'], $_POST['Gender'],
            $_POST['Number_of_limbs'], serialize($_POST['Superpowers']), $_POST['Biografia']));
    }
?>